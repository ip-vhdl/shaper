library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.top_pkg.all;

entity shaper_tb is
end entity shaper_tb;

architecture test_tb of shaper_tb is

  signal clk, rst : std_logic                                       := '1';
  signal samples  : signed(c_width-1 downto 0)                      := (others => '0');
  signal output   : signed(2*(samples'length+2)+2*c_acc-1 downto 0) := (others => '0');

begin

  DUT : entity work.shaper
    generic map (g_m1      => c_m1,
                 g_m2      => c_m2,
                 g_acc     => c_acc,
                 g_delay_1 => c_delay_1,
                 g_delay_2 => c_delay_2)
    port map (clk    => clk,
              rst    => rst,
              input  => samples,
              output => output);

  clk <= not clk after 5 ns;           -- clock generation

  U_samples : process (clk) is
    use std.textio.all;
    file mysamples   : text open read_mode is "samples/samples.txt";
    variable values  : line;
    variable sample  : integer;
    variable read_ok : boolean;
  begin
    if rising_edge(clk) then
      if rst = '0' then
        readline(mysamples, values);
        read (values, sample, read_ok);
        samples <= to_signed(sample, c_width);
      end if;
    end if;
  end process U_samples;

  WaveGen_Proc : process
  begin
    rst <= '1';
    wait for 200 ns;
    rst <= '0';
    wait;
  -- wait until clk = '1';
  end process WaveGen_Proc;

end architecture test_tb;

configuration shaper_tb_test_tb_cfg of shaper_tb is
  for test_tb
  end for;
end shaper_tb_test_tb_cfg;
